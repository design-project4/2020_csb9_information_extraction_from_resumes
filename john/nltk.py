import nltk
import pandas as pd

file = open("test_resume.txt")
line = file.read()
print(line)

nltk.download()

text = line


words = nltk.word_tokenize(text)
words


pos_tags = nltk.pos_tag(words)
pos_tags

chunks = nltk.ne_chunk(pos_tags,binary=False)
for chunk in chunks:
    print(chunk)

entities = []
labels = []
for chunk in chunks:
    if hasattr(chunk,'label'):
        entities.append(''.join(c[0] for c in chunk))
        labels.append(chunk.label())
    
entities_labels = list(set(zip(entities,labels)))
entities_df = pd.DataFrame(entities_labels)
entities_df.columns = ["Entities","Labels"]
entities_labels