Steps to follow  <br />
Step 1: Load the trained custom spacy model. <br />
Step 2: Read a resume in pdf or word format. <br />
Step 3: Convert the resume to text format. <br />
Step 4: Feed the text format resume to the trained model. <br />
Step 5: Store the resulting list named entities and corresponding custom labels to a variable. <br />
Step 6: Print the named entities and its corresponding custom labels. <br />

Requirements <br />
Libraries namely spaCy, <br />
nltk, <br />
pickle, <br />
random, <br />
PyMuPDF <br />
etc has to be imported prior to running the program. <br />
These Libraries can be installed using pip install lib-name. <br />
Python 3.7(3.8/3.9 also compatible) <br />

System Requirements <br />
512MB RAM  <br />
intel Pentium processor <br />
internet connection(optional) <br />
